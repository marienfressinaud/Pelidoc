# Pandoc Generator (Pelidoc)

A Pelican plugin to use Pandoc software.

Pandoc is a powerful tool to transform text from different formats (e.g. ReST,
Markdown) to others. It is really helpful to generate PDF or EPUB. It also can easily replace the "official" PDF plugin.

This plugin provides a powerful interface to handle document generation through
Pandoc.

**Important note:** this plugin is no longer maintained. The last time I tried, Pelidoc was working, but it doesn't mean it's still true.

## Installation

You need to have [Pandoc](http://johnmacfarlane.net/pandoc/) installed on your system.

Then, put the plugin directory in your plugin directory. For instance, create a `./plugins` directory in your Pelican project and add these lines in your `pelicanconf.py` file:

```python
PLUGIN_PATHS = ['plugins']
PLUGINS = ['Pelidoc']
```

Obviously, you'll have to adapt instructions if you already have installed some plugins.

To use this plugin then, you'll need to set a `PANDOC_OUTPUTS` configuration variable. It is a dictionary where the keys are the output formats and the
values are the corresponding destination directories. For instance:

```python
PANDOC_OUTPUTS = {
    'pdf': 'pdfs',
    'epub': 'epubs',
}
```

Note you'll have to modify your theme template to support download of files. Here is a snippet to download files (PDF or EPUB) of a specific article. You can put it in the `templates/article_infos.html` file (or similar) of your theme:

```jinja
{% if 'pdf' in PANDOC_OUTPUTS or 'epub' in PANDOC_OUTPUTS %}
    <div class="entry-download">
        {% if 'pdf' in PANDOC_OUTPUTS %}
            <a href="{{ SITEURL }}/{{ PANDOC_OUTPUTS['pdf'] }}/{{ article.slug }}.pdf">Download as PDF</a><br />
        {% endif %}
        {% if 'epub' in PANDOC_OUTPUTS %}
            <a href="{{ SITEURL }}/{{ PANDOC_OUTPUTS['epub'] }}/{{ article.slug }}.epub">Download as EPUB</a>
        {% endif %}
    </div>
{% endif %}
```

## How-to contribute

Don’t: this project is no longer maintained, but feel free to fork the repository!

## Credits

- [Pelican](http://getpelican.com) by [Alexis Métaireau](http://blog.notmyidea.org/) ;
- [Pelican plugins](https://github.com/getpelican/pelican-plugins) for the code basis (especially PDF plugin!)
- [Pandoc Reader](https://github.com/liob/pandoc_reader), another Pandoc plugin for Pelican but a bit less powerful, by [Hinrich B. Winther](https://github.com/liob).
- And, obviously, [Pandoc](http://johnmacfarlane.net/pandoc/) by [John MacFarlane](http://johnmacfarlane.net).
